package com.example.demodoma2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Demodoma2Application

fun main(args: Array<String>) {
	runApplication<Demodoma2Application>(*args)
}
